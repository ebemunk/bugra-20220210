import { configureStore, EnhancedStore } from "@reduxjs/toolkit";
import { render, RenderOptions } from "@testing-library/react";
import { ReactElement, ReactNode } from "react";
import { Provider } from "react-redux";
import { ThemeProvider } from "styled-components";
import theme from "../theme";
import orderbookReducer from "../features/orderbook/slice";
import { RootState } from "../store";

export const renderWithProviders = (
  ui: ReactElement,
  {
    preloadedState = {},
    store = configureStore({
      reducer: {
        orderbook: orderbookReducer,
      },
      preloadedState,
    }),
    ...options
  }: RenderOptions & {
    preloadedState?: Partial<RootState>;
    store?: EnhancedStore;
  } = {}
) => {
  function Wrapper({ children }: { children: ReactNode }) {
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>{children}</ThemeProvider>
      </Provider>
    );
  }

  return render(ui, {
    wrapper: Wrapper,
    ...options,
  });
};
