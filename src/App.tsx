import { store } from "./store";
import { Provider } from "react-redux";
import { ThemeProvider } from "styled-components";
import OrderBook from "./features/orderbook";
import theme, { GlobalStyle } from "./theme";

function App() {
  return (
    <>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <OrderBook />
        </ThemeProvider>
      </Provider>
    </>
  );
}

export default App;
