import useResizeObserver from "@react-hook/resize-observer";
import { debounce } from "lodash-es";
import { useLayoutEffect, useRef, useState } from "react";

export default function useElementSize<T extends HTMLElement>({
  debounceMs = 0,
}: {
  debounceMs?: number;
} = {}) {
  const [size, setSize] = useState<DOMRect | null>(null);
  const ref = useRef<T | null>(null);

  useLayoutEffect(() => {
    if (!ref.current) return;

    setSize(ref.current.getBoundingClientRect());
  }, []);

  useResizeObserver(
    ref,
    debounce((entry) => setSize(entry.contentRect), debounceMs)
  );

  return [ref, size] as const;
}
