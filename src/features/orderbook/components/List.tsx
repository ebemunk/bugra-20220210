import { ComponentPropsWithRef } from "react";
import styled from "styled-components";
import { PriceLevel } from "../slice";
import { formatCurrency, formatScalar } from "../util";

export const Rows = styled.div`
  width: 100%;
  font-family: ${(props) => props.theme.fonts.monospace};
  display: flex;
  flex-direction: column;
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  text-align: right;
  position: relative;
`;

export const HeaderRow = styled(Row)`
  color: ${(props) => props.theme.colors.gray};
  font-family: ${(props) => props.theme.fonts.sansSerif};
  font-size: 0.8rem;
  border-top: 2px solid ${(props) => props.theme.colors.border};
  border-bottom: 1px solid ${(props) => props.theme.colors.border};
  padding: 0.4rem;
`;

const Price = styled.div<{ highlightColor?: string }>`
  color: ${(props) => props.highlightColor ?? "inherit"};
  flex-basis: 33.3%;
  margin: 0 1.5rem;
`;

const Size = styled.div`
  flex-basis: 33.3%;
  margin: 0 1.5rem;
`;

const Total = styled.div`
  flex-basis: 33.3%;
  margin: 0 1.5rem;
`;

export const DepthBar = styled.div<{
  color: string;
}>`
  position: absolute;
  height: 100%;
  background-color: ${(props) => props.color};
  opacity: 0.3;
  z-index: -1;
  left: 0;
`;

export default function List({
  items,
  className,
  highlightColor,
  highestTotal,
  ...rest
}: {
  items: PriceLevel[];
  className?: string;
  highlightColor: string;
  highestTotal: number;
} & ComponentPropsWithRef<"div">) {
  return (
    <Rows className={className} {...rest}>
      <HeaderRow>
        <Price>PRICE</Price>
        <Size>SIZE</Size>
        <Total>TOTAL</Total>
      </HeaderRow>
      {items.map((item, i) => (
        <Row key={i}>
          <DepthBar
            style={{ width: `${(item[2] / highestTotal) * 100}%` }}
            color={highlightColor}
          />
          <Price highlightColor={highlightColor}>
            {formatCurrency(item[0])}
          </Price>
          <Size>{formatScalar(item[1])}</Size>
          <Total>{formatScalar(item[2])}</Total>
        </Row>
      ))}
    </Rows>
  );
}
