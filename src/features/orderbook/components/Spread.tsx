import { useSelector } from "react-redux";
import styled from "styled-components";
import { RootState } from "../../../store";
import { formatCurrency } from "../util";

const Wrapper = styled.div`
  color: ${(props) => props.theme.colors.gray};
  font-size: 0.8rem;
  text-align: center;
  margin: 0.4rem 0;
`;

const Monospace = styled.span`
  font-family: ${(props) => props.theme.fonts.monospace};
`;

export default function Spread({ className }: { className?: string }) {
  const maxBid = useSelector(
    (state: RootState) => state.orderbook.bids[0]?.[0]
  );
  const minAsk = useSelector(
    (state: RootState) => state.orderbook.asks[0]?.[0]
  );

  if (maxBid === undefined || minAsk === undefined) return null;

  const spread = Math.abs(minAsk - maxBid);
  const margin = (spread / minAsk) * 100;

  return (
    <Wrapper className={className} aria-label="Spread">
      Spread{" "}
      <Monospace>
        {formatCurrency(spread)} ({margin.toFixed(2)}%)
      </Monospace>
    </Wrapper>
  );
}
