import styled from "styled-components";
import List, { DepthBar, Row, HeaderRow } from "./List";

const Bids = styled(List)`
  flex-basis: 50%;
  order: 0;

  @media (min-width: ${(props) => props.theme.breakpoints.sm + 1}px) {
    & ${DepthBar} {
      right: 0;
      left: initial;
    }

    & ${Row} {
      flex-direction: row-reverse;
    }
  }

  @media (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    order: 2;

    & ${HeaderRow} {
      display: none;
    }
  }
`;

export default Bids;
