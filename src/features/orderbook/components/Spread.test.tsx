import { screen } from "@testing-library/react";
import { renderWithProviders } from "../../../test/helpers";
import { ProductId, WebsocketReadyState } from "../slice";
import Spread from "./Spread";

test("does not show spread if not available", () => {
  renderWithProviders(<Spread />, {
    preloadedState: {
      orderbook: {
        asks: [],
        bids: [],
        product_id: ProductId.PI_ETHUSD,
        readyState: WebsocketReadyState.CONNECTING,
        maxRows: 16,
      },
    },
  });

  expect(screen.queryByText(/Spread/)).not.toBeInTheDocument();
});

test("calculates the spread correctly", () => {
  renderWithProviders(<Spread />, {
    preloadedState: {
      orderbook: {
        asks: [
          [44500, 1, 1],
          [44501, 1, 2],
          [44502, 1, 3],
        ],
        bids: [
          [44490, 1, 1],
          [44489, 1, 2],
          [44488, 1, 3],
        ],
        product_id: ProductId.PI_ETHUSD,
        readyState: WebsocketReadyState.CONNECTING,
        maxRows: 16,
      },
    },
  });

  expect(screen.getByLabelText("Spread").textContent).toMatchInlineSnapshot(`"Spread 10.00 (0.02%)"`);
});
