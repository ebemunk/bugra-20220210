import styled from "styled-components";
import List, { HeaderRow } from "./List";

const Asks = styled(List)`
  flex-basis: 50%;
  order: 1;

  @media (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    order: 0;
    flex-direction: column-reverse;

    & ${HeaderRow} {
      order: 99;
    }
  }
`;

export default Asks;
