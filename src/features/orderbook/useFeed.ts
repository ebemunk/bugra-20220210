import { useCallback, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import {
  delta,
  PriceSize,
  setProductId,
  setReadyState,
  snapshot,
  WebsocketReadyState,
  ProductId,
} from "./slice";

type FeedEvent =
  | {
      event: "info";
      version: number;
    }
  | { event: "subscribed"; feed: "book_ui_1"; product_ids: ProductId[] };

type FeedMessage =
  | {
      feed: "book_ui_1_snapshot";
      numLevels: number;
      product_id: ProductId;
      asks: PriceSize[];
      bids: PriceSize[];
    }
  | {
      feed: "book_ui_1";
      product_id: ProductId;
      asks: PriceSize[];
      bids: PriceSize[];
    };

function isFeedEvent(action: FeedEvent | FeedMessage): action is FeedEvent {
  return "event" in action;
}

export default function useFeed() {
  const state = useSelector((state: RootState) => state.orderbook);
  const dispatch = useDispatch();
  const ws = useRef<WebSocket | null>(null);

  const connect = useCallback(() => {
    ws.current = new WebSocket("wss://www.cryptofacilities.com/ws/v1");

    ws.current.addEventListener("open", () => {
      dispatch(setReadyState(WebsocketReadyState.OPEN));
    });

    ws.current.addEventListener("message", (evt) => {
      try {
        const json = JSON.parse(evt.data) as FeedEvent | FeedMessage;

        if (isFeedEvent(json)) {
          return;
        }

        if (json.feed === "book_ui_1_snapshot") {
          dispatch(snapshot(json));
        } else {
          dispatch(delta(json));
        }
      } catch (err) {
        throw new Error(`could not parse websocket feed data: ${err}`);
      }
    });

    ws.current.addEventListener("close", () => {
      dispatch(setReadyState(WebsocketReadyState.CLOSED));
    });

    ws.current.addEventListener("error", (evt) => {
      console.log("errored", evt);
    });
  }, [dispatch]);

  const disconnect = useCallback(() => {
    if (!ws.current) return;

    ws.current.close();

    dispatch(setReadyState(WebsocketReadyState.CLOSING));
  }, [dispatch]);

  const subscribe = useCallback(() => {
    if (!ws.current) return;

    ws.current.send(
      JSON.stringify({
        event: "subscribe",
        feed: "book_ui_1",
        product_ids: [state.product_id],
      })
    );
  }, [state.product_id]);

  const prevProductId = useRef<ProductId>(state.product_id);
  useEffect(() => {
    if (!ws.current) {
      return;
    }

    if (state.readyState !== WebsocketReadyState.OPEN) {
      return;
    }

    if (state.product_id !== prevProductId.current) {
      ws.current.send(
        JSON.stringify({
          event: "unsubscribe",
          feed: "book_ui_1",
          product_ids: [prevProductId.current],
        })
      );
      prevProductId.current = state.product_id;
    }

    ws.current.send(
      JSON.stringify({
        event: "subscribe",
        feed: "book_ui_1",
        product_ids: [state.product_id],
        // numLevels: 100,
      })
    );
  }, [state.product_id, state.readyState]);

  const toggleFeed = useCallback(() => {
    const newProductId =
      state.product_id === ProductId.PI_ETHUSD
        ? ProductId.PI_XBTUSD
        : ProductId.PI_ETHUSD;

    dispatch(setProductId(newProductId));
  }, [state.product_id, dispatch]);

  useEffect(() => {
    const blurListener = () => {
      disconnect();
    };

    window.addEventListener("blur", blurListener);

    return () => {
      window.removeEventListener("blur", blurListener);
    };
  }, [disconnect]);

  return {
    connect,
    disconnect,
    subscribe,
    readyState: state.readyState,
    toggleFeed,
  };
}
