import { calculatePriceLevels, applyDelta, PriceSize } from "./slice";

describe("calculatePriceLevels", () => {
  test("it calculates the totals correctly", () => {
    const levels = calculatePriceLevels(
      [
        [10, 1],
        [11, 5],
        [15, 30],
        [16, 1],
        [17, 6],
      ],
      (a, b) => a[0] - b[0]
    );

    expect(levels).toEqual([
      [10, 1, 1],
      [11, 5, 6],
      [15, 30, 36],
      [16, 1, 37],
      [17, 6, 43],
    ]);
  });

  test("totals are correct with respect to sorting", () => {
    const levels = calculatePriceLevels(
      [
        [10, 1],
        [11, 5],
        [15, 30],
        [16, 1],
        [17, 6],
      ],
      (a, b) => b[0] - a[0]
    );

    expect(levels).toEqual([
      [17, 6, 6],
      [16, 1, 7],
      [15, 30, 37],
      [11, 5, 42],
      [10, 1, 43],
    ]);
  });

  test.each([
    [
      [
        [1, 1],
        [2, 1],
        [3, 1],
      ] as PriceSize[],
      3,
    ],
    [[], 0],
  ])(`output has same number of elements as input: %p - %p`, (arr, expected) =>
    expect(calculatePriceLevels(arr, (a, b) => b[0] - a[0]).length).toEqual(
      expected
    )
  );
});

describe("applyDelta", () => {
  test("removes price levels with 0 size from the array and ignores size 0", () => {
    const updated = applyDelta(
      [
        [1, 1, 1],
        [2, 1, 2],
        [3, 1, 3],
      ],
      [
        [1, 0],
        [3, 0],
        [4, 0],
      ]
    );

    expect(updated).toEqual([[2, 1]]);
  });

  test("replaces price levels that are not 0", () => {
    const updated = applyDelta(
      [
        [1, 1, 1],
        [2, 1, 2],
        [3, 1, 3],
      ],
      [[2, 3]]
    );

    expect(updated).toEqual([
      [1, 1],
      [2, 3],
      [3, 1],
    ]);
  });

  test("adds new price levels not already in the array", () => {
    const updated = applyDelta(
      [
        [1, 1, 1],
        [3, 1, 3],
      ],
      [[2, 1]]
    );

    expect(updated).toEqual([
      [1, 1],
      [3, 1],
      [2, 1],
    ]);
  });
});
