export const formatScalar = new Intl.NumberFormat("en-US").format;

export const formatCurrency = (num: number) =>
  num.toLocaleString("en-US", { minimumFractionDigits: 2 });
