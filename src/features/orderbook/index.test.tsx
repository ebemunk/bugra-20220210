import { fireEvent, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import WS from "jest-websocket-mock";
import OrderBook from ".";
import { renderWithProviders } from "../../test/helpers";
import { ProductId } from "./slice";

let ws: WS;
beforeEach(() => {
  ws = new WS("wss://www.cryptofacilities.com/ws/v1");
});
afterEach(() => {
  WS.clean();
});

test("happy path functionalities", async () => {
  renderWithProviders(<OrderBook />);

  // only contains headers before websocket is connected
  expect(screen.getByLabelText("Bids").childElementCount).toBe(1);
  expect(screen.getByLabelText("Asks").childElementCount).toBe(1);

  await ws.connected;

  // subscribes to the feed on mount
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "subscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_XBTUSD],
    })
  );

  // socket responds back with a snapshot
  await ws.send(
    JSON.stringify({
      feed: "book_ui_1_snapshot",
      numLevels: 25,
      product_id: ProductId.PI_XBTUSD,
      asks: [
        [10, 1],
        [11, 3],
        [12, 5],
        [13, 8],
      ],
      bids: [
        [9, 1],
        [8, 3],
        [7, 5],
      ],
    })
  );

  // make sure the lists are showing the correct number of rows
  expect(screen.getByLabelText("Bids").childElementCount).toBe(4);
  expect(screen.getByLabelText("Asks").childElementCount).toBe(5);

  await ws.send(
    JSON.stringify({
      feed: "book_ui_1",
      product_id: [ProductId.PI_XBTUSD],
      asks: [
        [10, 0],
        [11, 4],
        [12, 0],
      ],
      bids: [],
    })
  );

  // make sure the lists are showing the correct number of rows
  expect(screen.getByLabelText("Bids").childElementCount).toBe(4);
  expect(screen.getByLabelText("Asks").childElementCount).toBe(3);

  // click on Toggle Feed button
  userEvent.click(screen.getByRole("button", { name: "Toggle Feed" }));

  // make sure we reuse the ws by unsubscribing and then subscribing to the new product_id
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "unsubscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_XBTUSD],
    })
  );
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "subscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_ETHUSD],
    })
  );

  // overlay should show when user blurs window
  expect(screen.queryByTestId("disconnect-overlay")).not.toBeInTheDocument();

  fireEvent.blur(window);

  await ws.closed;

  expect(screen.getByTestId("disconnect-overlay")).toBeInTheDocument();

  // clicking on reconnect establishes a new websocket connection
  userEvent.click(screen.getByRole("button", { name: "Reconnect" }));

  await ws.connected;
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "subscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_ETHUSD],
    })
  );
  expect(screen.queryByTestId("disconnect-overlay")).not.toBeInTheDocument();
});

test("toggle button also works when disconnected", async () => {
  renderWithProviders(<OrderBook />);

  await ws.connected;
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "subscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_XBTUSD],
    })
  );

  fireEvent.blur(window);

  await ws.closed;

  userEvent.click(screen.getByRole("button", { name: "Toggle Feed" }));
  userEvent.click(screen.getByRole("button", { name: "Reconnect" }));

  await ws.connected;
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "unsubscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_XBTUSD],
    })
  );
  await expect(ws).toReceiveMessage(
    JSON.stringify({
      event: "subscribe",
      feed: "book_ui_1",
      product_ids: [ProductId.PI_ETHUSD],
    })
  );
});
