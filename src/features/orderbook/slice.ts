import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export enum ProductId {
  PI_XBTUSD = "PI_XBTUSD",
  PI_ETHUSD = "PI_ETHUSD",
}

export enum WebsocketReadyState {
  CONNECTING,
  OPEN,
  CLOSING,
  CLOSED,
}

export type PriceLevel = [number, number, number];
export type PriceSize = [number, number];

export interface OrderBookState {
  product_id: ProductId;
  readyState: WebsocketReadyState;
  asks: PriceLevel[];
  bids: PriceLevel[];
  maxRows: number;
}

export function calculatePriceLevels(
  arr: PriceSize[],
  sort: (a: PriceSize, b: PriceSize) => number
): PriceLevel[] {
  const levels = [];
  let total = 0;

  for (const [price, size] of arr.slice().sort(sort)) {
    total += size;
    levels.push([price, size, total] as PriceLevel);
  }

  return levels;
}

export function applyDelta(
  levels: PriceLevel[],
  deltas: PriceSize[]
): PriceSize[] {
  const arr = levels.slice().map(([price, size]) => [price, size] as PriceSize);
  for (const delta of deltas) {
    const idx = arr.findIndex((ask) => ask[0] === delta[0]);
    if (delta[1] === 0 && idx > -1) {
      arr.splice(idx, 1);
    } else if (delta[1] === 0 && idx < 0) {
      continue;
    } else if (idx > -1) {
      arr[idx] = delta;
    } else {
      arr.push(delta);
    }
  }
  return arr;
}

const initialState: OrderBookState = {
  product_id: ProductId.PI_XBTUSD,
  readyState: WebsocketReadyState.CONNECTING,
  asks: [],
  bids: [],
  maxRows: 16,
};

export const slice = createSlice({
  name: "orderbook",
  initialState,
  reducers: {
    setReadyState: (state, action: PayloadAction<WebsocketReadyState>) => {
      if (
        state.readyState === WebsocketReadyState.CLOSED &&
        action.payload === WebsocketReadyState.CLOSING
      )
        return state;

      state.readyState = action.payload;
    },
    setProductId: (state, action: PayloadAction<ProductId>) => {
      state.product_id = action.payload;
    },
    setMaxRows: (state, action: PayloadAction<number>) => {
      state.maxRows = action.payload;
    },
    snapshot: (
      state,
      action: PayloadAction<{
        product_id: ProductId;
        asks: PriceSize[];
        bids: PriceSize[];
      }>
    ) => {
      const { asks, bids } = action.payload;
      state.asks = calculatePriceLevels(asks, (a, b) => a[0] - b[0]);
      state.bids = calculatePriceLevels(bids, (a, b) => b[0] - a[0]);
    },
    delta: (
      state,
      action: PayloadAction<{
        asks: PriceSize[];
        bids: PriceSize[];
      }>
    ) => {
      const { asks, bids } = action.payload;
      state.asks = calculatePriceLevels(
        applyDelta(state.asks, asks),
        (a, b) => a[0] - b[0]
      );
      state.bids = calculatePriceLevels(
        applyDelta(state.bids, bids),
        (a, b) => b[0] - a[0]
      );
    },
  },
});

export const { setReadyState, setProductId, setMaxRows, snapshot, delta } =
  slice.actions;

export default slice.reducer;
