import { useCallback, useEffect, useMemo } from "react";
import { useSelector } from "react-redux";
import styled, { useTheme } from "styled-components";
import Button from "../../components/Button";
import Overlay from "../../components/Overlay";
import useElementSize from "../../hooks/useElementSize";
import { RootState } from "../../store";
import Asks from "./components/Asks";
import Bids from "./components/Bids";
import Spread from "./components/Spread";
import { WebsocketReadyState } from "./slice";
import useFeed from "./useFeed";

const Wrapper = styled.section``;

const Header = styled.header`
  padding: 0.6rem;
`;

const Book = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: flex-start;
  position: relative;

  @media (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    flex-direction: column;
  }
`;

const SpreadSM = styled(Spread)`
  order: 1;
  width: 100%;
`;

const SpreadLG = styled(Spread)`
  position: absolute;
  top: 0.3rem;
  left: 50%;
  transform: translateX(-50%);
`;

const ButtonRow = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1rem;
`;

export default function OrderBook() {
  const { readyState, connect, disconnect, toggleFeed } = useFeed();
  useEffect(() => {
    connect();

    return () => {
      disconnect();
    };
  }, [connect, disconnect]);

  const [asks, bids] = useSelector((state: RootState) => [
    state.orderbook.asks.slice(0, state.orderbook.maxRows),
    state.orderbook.bids.slice(0, state.orderbook.maxRows),
  ]);
  const highestTotal = useMemo(
    () => Math.max(asks.slice(-1)[0]?.[2] ?? 0, bids.slice(-1)[0]?.[2] ?? 0),
    [asks, bids]
  );

  const theme = useTheme();
  const [ref, size] = useElementSize<HTMLDivElement>();
  const isSmLayout = (size?.width ?? 0) < theme.breakpoints.sm;

  return (
    <Wrapper>
      <Header>Order Book{!isSmLayout && <SpreadLG />}</Header>
      <Book ref={ref}>
        <Bids
          items={bids}
          highlightColor={theme.colors.bid}
          highestTotal={highestTotal}
          aria-label="Bids"
        />
        {isSmLayout && <SpreadSM />}
        <Asks
          items={asks}
          highlightColor={theme.colors.ask}
          highestTotal={highestTotal}
          aria-label="Asks"
        />
        {readyState === WebsocketReadyState.CLOSED && (
          <Overlay data-testid="disconnect-overlay">
            <Button variant="primary" onClick={connect}>
              Reconnect
            </Button>
          </Overlay>
        )}
      </Book>
      <ButtonRow>
        <Button
          onClick={useCallback(() => {
            toggleFeed();
          }, [toggleFeed])}
          variant="primary"
        >
          Toggle Feed
        </Button>
      </ButtonRow>
    </Wrapper>
  );
}
