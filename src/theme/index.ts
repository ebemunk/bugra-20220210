import { createGlobalStyle, DefaultTheme } from "styled-components";

const theme: DefaultTheme = {
  colors: {
    bg: "rgb(19, 24, 38)",
    border: "rgb(35, 40, 55)",
    offwhite: "rgb(210, 210, 210)",
    gray: "rgb(130, 130, 130)",

    bid: "rgba(20,135,100)",
    ask: "rgba(180,50,55)",
    primary: "rgb(87, 65, 217)",
  },
  breakpoints: {
    sm: 768,
  },
  fonts: {
    serif: "serif",
    sansSerif: "sans-serif",
    monospace: "monospace",
  },
};

export default theme;

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: ${(props) => props.theme.fonts.sansSerif};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    background-color: ${(props) => props.theme.colors.bg};
    color: ${(props) => props.theme.colors.offwhite};
  }
`;
