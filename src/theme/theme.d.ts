import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    colors: {
      bg: string;
      border: string;
      offwhite: string;
      gray: string;

      ask: string;
      bid: string;
      primary: string;
    };
    breakpoints: {
      [key: string]: number;
    };
    fonts: {
      serif: string;
      sansSerif: string;
      monospace: string;
    };
  }
}
