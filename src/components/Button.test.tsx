import { screen } from "@testing-library/react";
import { renderWithProviders } from "../test/helpers";
import Button from "./Button";

test("has primary variant", () => {
  renderWithProviders(<Button variant="primary">Toggle Feed</Button>);

  expect(screen.getByRole("button")).toMatchInlineSnapshot(`
.c0 {
  border: none;
  border-radius: 5px;
  padding: 0.6rem;
  background-color: rgb(87,65,217);
  color: rgb(210,210,210);
}

.c0:hover {
  cursor: pointer;
}

<button
  class="c0"
>
  Toggle Feed
</button>
`);
});

test("has default variant", () => {
  renderWithProviders(<Button>hello</Button>);

  expect(screen.getByRole("button")).toMatchInlineSnapshot(`
.c0 {
  border: none;
  border-radius: 5px;
  padding: 0.6rem;
}

.c0:hover {
  cursor: pointer;
}

<button
  class="c0"
>
  hello
</button>
`);
});
