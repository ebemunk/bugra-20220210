import styled, { css } from "styled-components";

const Button = styled.button<{
  variant?: "primary";
}>`
  border: none;
  border-radius: 5px;
  padding: 0.6rem;

  &:hover {
    cursor: pointer;
  }

  ${(props) => {
    switch (props.variant) {
      case "primary":
        return css`
          background-color: ${props.theme.colors.primary};
          color: ${props.theme.colors.offwhite};
        `;
      default:
        return "";
    }
  }}
`;

export default Button;
